<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TodoTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function puedoVerElComponenteTodo()
    {
        $response = $this->get('/');

        $response->assertStatus(200)
            ->assertSeeLivewire('todo')
            ->assertSee('Mi lista de tareas CRUD');
    }
}
