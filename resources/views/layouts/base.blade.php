<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | Laravel CRUD</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    @livewireStyles
</head>

<body>
    <div class="container-fluid">
        <nav>
            <div class="nav-wrapper blue darken-4">
                <a href="#!" class="brand-logo">
                    <img src="https://bancamiga-portal-2020.s3.amazonaws.com/wp-content/uploads/2020/01/LogoPeq.png" alt="Bancamiga">
                </a>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a href="#!">
                            <i class="material-icons left">dashboard</i>
                            Home
                        </a>
                    </li>
                    <li><a href="#!"><i class="material-icons left">phone</i>Contact</a></li>
                    <li><a href="#!"><i class="material-icons left">info</i>About</a></li>
                </ul>
            </div>
        </nav>
        <div class="row">
            <div class="col s12 white-text blue darken-4 p4 center">
                <header>
                    <h1>Mi lista de tareas CRUD</h1>
                    <p class="flow-text">Una app de prueba para ver el flujo de CI/CD</p>
                </header>
            </div>
        </div>
        <div class="container">
            @yield('content')
        </div>
    </div>
    @livewireScripts
</body>

</html>
